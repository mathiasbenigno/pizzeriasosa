function check_contact() {
    
    $("#name_contact_c").keyup(function () {

        var name_contact = $(this).val();
        var checkData = {
            "nombre": name_contact
        };
        var checkreg = /^[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}( {1}[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20})?$/;
        prechecknombre();
        function prechecknombre() {
            if (name_contact == '') {
                $("#check_name_contact").removeClass();
                $("#check_name_contact").css("color", "");
            }
            else if (!checkreg.test(name_contact)) {
                $("#check_name_contact").addClass("fas fa-times");
                $("#check_name_contact").css("color", "");
            }
            else {
                $("#check_name_contact").removeClass("fas fa-times");
                $("#check_name_contact").addClass("fas fa-check");
                $("#check_name_contact").css("color", "");
                $.ajax({
                    url: "/check_name_consulta",
                    method: "POST",
                    data: checkData
                }).done(function (resp) {

                    //$("#loading-icon-2").hide();
                    if (resp == 'valido') {
                        console.log("valido");
                        $("#check_name_contact").css("color", "green");
                    }
                    else if (resp == 'no valido') {
                        console.log("no valido");
                        $("#check_name_contact").css("color", "red");
                    }
                });
            }
        }
    });
    $("#comment_contact_c").keyup(function () {

        var comment_contact = $(this).val();
        var checkData = {
            "comment": comment_contact
        };
        var checkreg = /^[a-zA-ZñÑéÉíÍóÓúÚáÁ\s¿¡!$#@.,?]{3,280}?$/;
        precheckcomment();
        function precheckcomment() {
            if (comment_contact == '') {
                $("#check_comment_contact").removeClass();
                $("#check_comment_contact").css("color", "");
            }
            else if (!checkreg.test(comment_contact)) {
                $("#check_comment_contact").addClass("fas fa-times");
                $("#check_comment_contact").css("color", "");
            }
            else {
                console.log("else comment");
                $("#check_comment_contact").removeClass("fas fa-times");
                $("#check_comment_contact").addClass("fas fa-check");
                $("#check_comment_contact").css("color", "");
                $.ajax({
                    url: "/check_comment_consulta",
                    method: "POST",
                    data: checkData
                }).done(function (resp) {
                    if (resp == 'valido') {
                        console.log("valido");
                        $("#check_comment_contact").css("color", "green");
                    }
                    else if (resp == 'no valido') {
                        console.log("no valido");
                        $("#check_comment_contact").css("color", "red");
                    }
                });
            }
        }
    });
    $("#email_contact_c").keyup(function () {
        console.log("email contact");
        var email_contact = $(this).val();

        var checkData = {
            "email": email_contact
        };
        var checkreg = /^[1-9a-zA-Z._^]{3,50}@{1}[a-zA-Z]{3,50}[.]{1}[a-zA-Z]{1,10}([.]{1}[a-zA-Z]{1,5})?$/;
        precheckemail();

        function precheckemail() {
            if (email_contact == '') {
                $("#check_email_contact").removeClass();
                $("#check_email_contact").css("color", "");
            }
            else if (!checkreg.test(email_contact)) {
                $("#check_email_contact").addClass("fas fa-times");
                $("#check_email_contact").css("color", "");
            }
            else {
                $("#check_email_contact").removeClass("fas fa-times");
                $("#check_email_contact").addClass("fas fa-check");
                $("#check_email_contact").css("color", "");
                $.ajax({
                    url: "/check_email_consulta",
                    method: "POST",
                    data: checkData
                }).done(function (resp) {
                    console.log("kasjdkasjd");
                    //$("#loading-icon-2").hide();
                    if (resp == 'valido') {
                        console.log("valido");
                        $("#check_email_contact").css("color", "green");
                    }
                    else if (resp == 'no valido') {
                        console.log("no valido");
                        $("#check_email_contact").css("color", "red");
                    }
                });
            }
        }
    });

    $("#phone_contact_c").keyup(function () {
        var phone_contact = $(this).val();

        var checkData = {
            "phone": phone_contact
        };
        var checkreg = /^[+]{1}[5]{1}[6]{1}[0-9]{9}$/;
        precheckphone();

        function precheckphone() {


            if (phone_contact == '') {

                console.log("if");
                $("#check_phone_contact").removeClass();
                $("#check_phone_contact").css("color", "");
            }
            else if (!checkreg.test(phone_contact)) {
                console.log("else if");
                $("#check_phone_contact").addClass("fas fa-times");
                $("#check_phone_contact").css("color", "");
            }
            else {
                $("#check_phone_contact").removeClass("fas fa-times");
                $("#check_phone_contact").addClass("fas fa-check");
                $("#check_phone_contact").css("color", "");
                $.ajax({
                    url: "/check_phone_consulta",
                    method: "POST",
                    data: checkData
                }).done(function (resp) {
                    if (resp == 'valido') {
                        console.log("valido");
                        $("#check_phone_contact").css("color", "green");
                    }
                    else if (resp == 'no valido') {
                        console.log("no valido");
                        $("#check_phone_contact").css("color", "red");
                    }
                });
            }
        }
    });
    

}
function submit_contact() {


    $("#submit_contact").click(function (e) {
        e.preventDefault();
        console.log("entro en submit contact");
        var nombre = $("#name_contact_c").val();
        var email = $("#email_contact_c").val();
        var phone = $("#phone_contact_c").val();
        var comment = $("#comment_contact_c").val();

        var checkData = {
            "nombre": nombre,
            "email": email,
            "phone": phone,
            "comment": comment
        };

        var checkreg_nombre = /^[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}( {1}[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20})?$/;
        var checkreg_phone = /^[+]{1}[5]{1}[6]{1}[0-9]{9,9}$/;
        var checkreg_email = /^[1-9a-zA-Z._^]{3,50}@{1}[a-zA-Z]{3,50}[.]{1}[a-zA-Z]{1,10}([.]{1}[a-zA-Z]{1,5})?$/;
        var checkreg_comment = /^[a-zA-ZñÑéÉíÍóÓúÚáÁ¿¡!$#@.,?\s]{3,280}?$/;
        if (nombre == '' || (!checkreg_nombre.test(nombre)) || email == '' || (!checkreg_email.test(email)) || phone == '' || (!checkreg_phone.test(phone)) || comment == '' || (!checkreg_comment.test(comment))) {
            alert('Alguno de los campos estan vacios o estan escritos con algun caracter invalido!');

        } else {
            console.log('entro en el else ajx');
            $.ajax({
                url: "/submit_contact",
                method: "POST",
                data: checkData
            }).done(function (resp) {
                if (resp == 'valido') {
                    alert("Tu consulta ha sido enviada con exito");
                } else if (resp == 'no valido') {
                    alert("UPS parece que faltan datos, formulario no completo");
                }

            });
        }

    })
}