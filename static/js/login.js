    function login_cookie() {
        const cookieExists = Cookies.get('loginCookie');
        if (cookieExists) {
            displayname = Cookies.get('UserConect');
            $("#login_display_name").removeClass("hide").show().html("Bienvenido! " + displayname);
            $("#logout").removeClass("hide").show();
            $("#login_pedir_comida").removeClass("hide").show();
            $("#login_main").addClass("hide").hide();
            $("#signup").addClass("hide").hide("slow");
            $("#last_b").addClass("hide").hide("slow");
            $("#last_a").css("display", "none");
            $("#fb_login").hide();
            ////////////////////////////////////////////////////////////
            $("#login").css("display", "none");
            $("#login_main").css("display", "none");
            $("main").css("flex-direction", "row");
            $("main").css("justify-content", "center");
            //$("main").css("height", "inherit");
            $("#login_display_name").css("display", "none");
            /////////////////////////////////////////////////////////////
            /*window.location.hash.split('#');*/
            window.history.pushState('Home', 'Bienvenido a Pizzeria Sosa', '/home');
            $.ajax({
                url: "/_get_home",
                type: "POST",
                success: function (resp) {
                    $('#div_welcome_home').replaceWith(resp.data);
                    $(".back_pizzas").addClass("hide").hide();
                    $(".add_to_pizza").addClass("hide").hide();
                    $.getScript("static/js/food.js");
                    select_food();
    
                }
            });
        }
        else {
            $("#login_display_name").addClass("hide").hide("slow");
            $("#logout").addClass("hide").hide("slow");
            $("#login_pedir_comida").addClass("hide").hide("slow");
            $(".login_main").removeClass("hide").show("slow");
            $("#signup").removeClass("hide").show("slow");
            $("#last_a").hide("slow");
            $("#fb_login").hide();
        }
    }
    function login() {
        $("#login_button").click(function () {
            var username_login = $("#username").val();
            var password_login = $("#password").val();
            var data_login = {
                "username": username_login,
                "password": password_login
            }
            $.ajax({
                dataType: "json",
                url: "/login",
                method: "POST",
                data: data_login
            }).done(function (resp) {
                if (resp["valido"]) {
                    Cookies.set('UserConect', resp["result"]);
                    displayname = Cookies.get('UserConect');
                    $("#login_display_name").html(displayname);
                    alert("Te acabas de logear con exito " + resp["result"]);
                    Cookies.set('loginCookie', 'login');
                    alert(document.cookie);
                    login_cookie();
                    $("main").css("flex-direction", "row");
                    $("main").css("height", "70px");
                    $("#login_display_name").css("display", "none");
                } else if (resp["no valido"]) {
                    
                    alert("Su email o contraseña no son validos, intentelo nuevamente");
                }
                else{
                    
                }
            });


        });
        $("#logout").click(function(e){
            e.preventDefault();
            Cookies.remove("UserConect");
            document.cookie = "loginCookie =;expires=Thu, 01 jan 1970 00:00:00 UTC;";
            $(this).fadeOut("slow)");
            $("#login_display_name").addClass("hide").html("").hide();
            $("#login_pedir_comida").addClass("hide").hide();
            $(".login_main").removeClass("hide").show();
            $("#last_a").hide("slow");
            $("#signup").removeClass("hide").show();
            $("#fb_login").hide();
            alert(document.cookie);
        })
    }

    


