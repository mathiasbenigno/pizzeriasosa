    function check_signup() {
    
        $("#name_contact").keyup(function () {
    
            var name_contact = $(this).val();
            var checkData = {
                "nombre": name_contact
            };
            var checkreg = /^[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}( {1}[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20})?$/;
            prechecknombre();
            function prechecknombre() {
                if (name_contact == '') {
                    $("#check_name").removeClass();
                    $("#check_name").css("color", "");
                }
                else if (!checkreg.test(name_contact)) {
                    $("#check_name").addClass("fas fa-times");
                    $("#check_name").css("color", "");
                }
                else {
                    $("#check_name").removeClass("fas fa-times");
                    $("#check_name").addClass("fas fa-check");
                    $("#check_name").css("color", "");
                    $.ajax({
                        url: "/check_name_consulta",
                        method: "POST",
                        data: checkData
                    }).done(function (resp) {
    
                        //$("#loading-icon-2").hide();
                        if (resp == 'valido') {
                            console.log("valido");
                            $("#check_name").css("color", "green");
                        }
                        else if (resp == 'no valido') {
                            console.log("no valido");
                            $("#check_name").css("color", "red");
                        }
                    });
                }
            }
        });
        $("#email_contact").keyup(function () {
            console.log("email contact");
            var email_contact = $(this).val();
    
            var checkData = {
                "email": email_contact
            };
            var checkreg = /^[1-9a-zA-Z._^]{3,50}@{1}[a-zA-Z]{3,50}[.]{1}[a-zA-Z]{1,10}([.]{1}[a-zA-Z]{1,5})?$/;
            precheckemail();
    
            function precheckemail() {
                if (email_contact == '') {
                    $("#check_email").removeClass();
                    $("#check_email").css("color", "");
                }
                else if (!checkreg.test(email_contact)) {
                    $("#check_email").addClass("fas fa-times");
                    $("#check_email").css("color", "");
                }
                else {
                    $("#check_email").removeClass("fas fa-times");
                    $("#check_email").addClass("fas fa-check");
                    $("#check_email").css("color", "");
                    $.ajax({
                        url: "/check_email_consulta",
                        method: "POST",
                        data: checkData
                    }).done(function (resp) {
                        console.log("kasjdkasjd");
                        //$("#loading-icon-2").hide();
                        if (resp == 'valido') {
                            console.log("valido");
                            $("#check_email").css("color", "green");
                        }
                        else if (resp == 'no valido') {
                            console.log("no valido");
                            $("#check_email").css("color", "red");
                        }
                    });
                }
            }
        });
    
        $("#phone_contact").keyup(function () {
            var phone_contact = $(this).val();
    
            var checkData = {
                "phone": phone_contact
            };
            var checkreg = /^[+]{1}[5]{1}[6]{1}[0-9]{9}$/;
            precheckphone();
    
            function precheckphone() {
    
    
                if (phone_contact == '') {
    
                    console.log("if");
                    $("#check_phone").removeClass();
                    $("#check_phone").css("color", "");
                }
                else if (!checkreg.test(phone_contact)) {
                    console.log("else if");
                    $("#check_phone").addClass("fas fa-times");
                    $("#check_phone").css("color", "");
                }
                else {
                    $("#check_phone").removeClass("fas fa-times");
                    $("#check_phone").addClass("fas fa-check");
                    $("#check_phone").css("color", "");
                    $.ajax({
                        url: "/check_phone_consulta",
                        method: "POST",
                        data: checkData
                    }).done(function (resp) {
                        if (resp == 'valido') {
                            console.log("valido");
                            $("#check_phone").css("color", "green");
                        }
                        else if (resp == 'no valido') {
                            console.log("no valido");
                            $("#check_phone").css("color", "red");
                        }
                    });
                }
            }
        });
        $("#password_contact").keyup(function () {
            var password_contact = $(this).val();
            var checkData = {
                "password": password_contact
            };
            var checkreg = /^[+@!_#$%&*a-zA-Z0-9]{5,30}$/;
            precheckpassword();
    
            function precheckpassword() {
    
                if (password_contact == '') {
    
                    console.log("if");
                    $("#check_password").removeClass();
                    $("#check_password").css("color", "");
                }
                else if (!checkreg.test(password_contact)) {
                    console.log("else if");
                    $("#check_password").addClass("fas fa-times");
                    $("#check_password").css("color", "");
                }
                else {
                    $("#check_password").removeClass("fas fa-times");
                    $("#check_password").addClass("fas fa-check");
                    $("#check_password").css("color", "");
                    $.ajax({
                        url: "/check_password_consulta",
                        method: "POST",
                        data: checkData
                    }).done(function (resp) {
                        if (resp == 'valido') {
                            console.log("valido");
                            $("#check_password").css("color", "green");
                        }
                        else if (resp == 'no valido') {
                            console.log("no valido");
                            $("#check_password").css("color", "red");
                        }
                    });
                }
            }
        });
    
        $("#password_contact2").keyup(function () {
            console.log("PASSWORD2");
            var password_contact2 = $(this).val();
            var password_contact = $("#password_contact").val();
    
            if (password_contact == password_contact2) {
                console.log("if pass 2");
                $("#check_password2").removeClass("fas fa-times");
                $("#check_password2").addClass("fas fa-check");
                $("#check_password2").css("color", "green");
    
            }
            else if (password_contact2 == "") {
                $("#check_password2").removeClass();
                $("#check_password2").css("color", "");
            }
            else if (password_contact != password_contact2) {
                $("#check_password2").addClass("fas fa-times");
                $("#check_password2").css("color", "");
            }
        })
    
    }
    function submit_register() {
        $("#submit_signup").click(function (e) {
            e.preventDefault();
            console.log("entro en submit contact");
            var nombre = $("#name_contact").val();
            var email = $("#email_contact").val();
            var phone = $("#phone_contact").val();
            var password = $("#password_contact").val();
            var password2 = $("#password_contact2").val();

            var checkData = {
                "nombre": nombre,
                "email": email,
                "phone": phone,
                "password": password
            };
    
            var checkreg_password = /^[+@!_#$%&*ñÑa-zA-Z0-9]{5,30}$/;
            var checkreg_nombre = /^[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20}( {1}[a-zA-ZñÑéÉíÍóÓúÚáÁ]{3,20})?$/;
            var checkreg_phone = /^[+]{1}[5]{1}[6]{1}[0-9]{9,9}$/;
            var checkreg_email = /^[1-9a-zA-Z._^]{3,50}@{1}[a-zA-Z]{3,50}[.]{1}[a-zA-Z]{1,10}([.]{1}[a-zA-Z]{1,5})?$/;
            if (nombre == '' || (!checkreg_nombre.test(nombre)) || email == '' || (!checkreg_email.test(email)) || phone == '' || (!checkreg_phone.test(phone)) || password == '' || (!checkreg_password.test(password)) || password2 == '' || (!checkreg_password.test(password2))) {
                alert('Alguno de los campos estan vacios o estan escritos con algun caracter invalido!');
    
            } else {
                console.log('entro en submit register');
                $.ajax({
                    url: "/submit_register",
                    method: "POST",
                    data: checkData
                }).done(function (resp) {

                    if (resp == 'valido') {
                        console.log("borrando name_contact");
                        $(".contact_input").val("");
                        $("#check_name, #check_email, #check_phone, #check_password, #check_password2").removeClass();
                        alert("Te acabas de registrar con exito");
                        
                    } else if (resp == 'no valido') {
                        
                        alert("Su registro no pudo ser completado con exito");
                    } else if (resp == 'correo ya usado') {
                        alert(`el email: (${email}) ya esa registrado!`);
                    }

    
                });
            }
    
        })
    }