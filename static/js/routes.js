miurles = window.location.pathname;
function init_route() { 
    $.getScript("static/js/login.js");
    login_cookie();
    login();
    $.getScript("static/js/validation_contact.js");
    check_contact();
    submit_contact();
    $(document).ready(function(){

                    var $window = $(window);
                    var $sector = $('#home_section_right');
                    var $main = $('main');
                    var $login_inputs = $('.login_main');
                    var $loginopen = $("#loginopen");
                    var $login = $("#login");
                    function checkWidth() {
                        var windowsize = $window.width();
                        if (windowsize < 1000) {
                            $loginopen.hide();
                            $sector.hide();
                            /*$main.css("height", "10%");*/
                           
                            $login_inputs.hide();
                            $("#login").click(function(e){
                                e.preventDefault();
                                $loginopen.show().css("visibility", "visible");
                                $login.hide();
                               
                                $login_inputs.show("Slow").css("visibility", "initial");
                                $main.css("height", "200px");
                            });
                            $("#loginopen").click(function(e){
                                $loginopen.hide().css("visibility", "hidden");
                                $login.show().css("visibility", "initial");
                                e.preventDefault();
                               
                                $login_inputs.hide("Slow").css("visibility", "initial");
                                $main.css("height", "50px");
                            });
                        }else if (windowsize > 1000){
                            $main.css("height", "50px");
                            $sector.show();
                            $login_inputs.show().removeClass("hide");
                        }
                        
                        }
                    checkWidth();
                    $(window).resize(checkWidth);
    })
                



    if (miurles == "/") {
        window.history.pushState('Home', 'Bienvenido a Pizzeria Sosa', '/home')
        $.ajax({
            url: "/_get_home",
            type: "POST",
            success: function (resp) {
                $('#div_welcome_home').replaceWith(resp.data);
                $.getScript("static/js/food.js");
                select_food();

            }
        });
    }
    else if (miurles == "/home") {
        window.history.pushState('Home', 'Bienvenido a Pizzeria Sosa', '/home')
        $.ajax({
            url: "/_get_home",
            type: "POST",
            success: function (resp) {
                $('#div_welcome_home').replaceWith(resp.data);
                $.getScript("static/js/food.js");
                select_food();
    
            }
        });
    }
    else if (miurles == "/about") {
        window.history.pushState('about', 'Acerca de Pizzeria Sosa', '/about');
        $.ajax({
            url: "/_get_about",
            type: "POST",
            success: function (resp) {
                $('#div_welcome_home').replaceWith(resp.data);

            }
        });
    }
    else if (miurles == "/signup") {
        window.history.pushState('Sign up!', 'Registrate en Pizzeria Sosa', '/signup');
        $.ajax({
            url: "/_get_signup",
            type: "POST",
            success: function (resp) {
                $('#div_welcome_home').replaceWith(resp.data);
                $.getScript("static/js/validation.js");
                check_signup();
                submit_register();
            }
        });
    }
}
/*function init() {
    
    window.location.hash.split('#');
}*/
/*function loadTemplate(tmpl) {
  
    window.location.hash = tmpl;
    //history.replaceState(null,null,'');
    /*
    fetch(`/${tmpl}`, { method: "POST" })
        .then((content) => {
            return content.json();
        })
        .then((content) => {
            document.getElementById('div_welcome_home').innerHTML = content.html;
        });
}*/
$(document).ready(function () {

    $("#about").click(function (e) {
        e.preventDefault();
        /*window.location.hash.split('#');*/
        window.history.pushState('About', 'About Pizzeria Sosa', '/about');
        $.ajax({
            url: "/_get_about",
            type: "POST",
            success: function (resp) {
                $('#div_welcome_home').replaceWith(resp.data);

            }
        });
    });
    $("#home").click(function (e) {
        e.preventDefault();
        /*window.location.hash.split('#');*/
        window.history.pushState('Home', 'Bienvenido a Pizzeria Sosa', '/home');
        $.ajax({
            url: "/_get_home",
            type: "POST",
            success: function (resp) {
                $('#div_welcome_home').replaceWith(resp.data);
                $.getScript("static/js/food.js");
                select_food();
            }
        });
    });
    $("#signup").click(function (e) {
        e.preventDefault();
        window.history.pushState('Registrate', 'Registrate para hacer tus pedidos', '/signup');
        $.ajax({
            url: "/_get_signup",
            type: "POST",
            success: function (resp) {
                $('#div_welcome_home').replaceWith(resp.data);
                $.getScript("static/js/validation.js");
                check_signup();
                submit_register();

            }
        });
    });
});