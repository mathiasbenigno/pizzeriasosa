function select_food() {
    
    $("#checkout_box").focus();
    //$(".aceitunas_verdes_").css("display", "none"); //agregar
    //$(".pimenton_verde_").css("display", "none"); //agregar
    //$(".pepperoni_").css("display", "none"); //agregar
    //$(".aceitunas_negras_").css("display", "none"); //agregar
    $(".camarones_").css("display", "none"); //agregar
    $(".palta_").css("display", "none"); //agregar
    
    /*$("#hotdogs").click(function(){
        $("#hotdogs_all").css("display", "flex");
        $("#hotdogs_all").css("visibility", "visible");
    });*/

    function waitingorder() {
        if (addAllPizzaJson.cantidad_total == 1) {
            $("#waiting_order").html(45);
            $("#type_time").html("Minutos" + "<i class='fas fa-stopwatch'></i>");
            all_pedidosJson["Pedido"]["Espera"] = "45 minutos";
        } else if (addAllPizzaJson.cantidad_total == 2) {

            $("#waiting_order").html(60);
            $("#type_time").html("Minutos" + "<i class='fas fa-stopwatch'></i>");
            all_pedidosJson["Pedido"]["Espera"] = "60 minutos";
        } else if (addAllPizzaJson.cantidad_total == 3) {

            $("#waiting_order").html("+ de 1:20");
            $("#type_time").html("Horas" + "<i class='fas fa-stopwatch'></i>");
            all_pedidosJson["Pedido"]["Espera"] = "+ de 1:20 horas";
        } else if (addAllPizzaJson.cantidad_total > 3) {
            $("#waiting_order").html("+ de 1:30" + "<i class='fas fa-stopwatch'></i>");
            $("#type_time").html("Horas");
            all_pedidosJson["Pedido"]["Espera"] = "+ de 1:30";
        }
    }
    function ShowTotal() {
        if (addAllPizzaJson.Total == 0) {
            $(".contador_in").hide();

        } else {

            $(".contador_in").show();
        }
    }
    pizzajson = {};
    ingredienteJson = {
        "Ingredientes": "(base de salsa)",
        "Muzzarella": 490, "Pepperoni": 490, "Aceitunas_negras": 390,
        "Aceitunas_verdes": 390, "Pimenton_rojo": 390,
        "Pimenton_verde": 390, "Cebolla": 390, "Tocino": 990, "Camarones": 990,
        "Champiñones": 390, "Salame": 490, "Jamon": 490, "Tomate": 390, "Palta":990, "Choclo":390, "Mayonesa": 0, "Ketchup": 0, "Mostaza": 0,
        "Cebolla_hot": 390, "Salsa_premium": 0, "lechuga_hot": 190, "Tocino_hot": 490, "Pepinillos_hot": 290, "Tomate_hot": 190, "Palta_hot": 590, "Choclo_hot":390, "Queso_cheddar":290, "Huevo":290, "Queso_hot":290
    };
    
    ingredienteExtra = {
         
    };
    selectIngJson = {};

    selectPizzaJson = {};

    addAllPizzaJson = {
        "total_temporal": "0", "Total": "0",
        "Ingredientes": "(base de salsa)",
        "Pizza": "",
        "AddPizza": "",
        "add_hamburguesa_cantidad": "0",
        "add_hamburguesa_nombre": "Promo Hamburgesa",
        "add_individual_cantidad": "0",
        "add_individual_nombre": "Pizza individual",
        "add_mediana_cantidad": "0",
        "add_mediana_nombre": "Pizza Mediana",
        "add_familiar_cantidad": "0",
        "add_familiar_nombre": "Pizza familiar",
        "add_pizzaxl_cantidad": "0",
        "add_pizzaxl_nombre": "Pizza xl",
        "cantidad_total": "0",
        "cantidad_total_history": "0",
        "add_hamburguesa": "1620",
        "add_individual": "699",
        "add_mediana": "2990",
        "add_familiar": "4990",
        "add_pizzaxl": "6990",
        "add_individual_id_nombre": "PizzaIndividual",
        "add_mediana_id_nombre": "PizzaMediana",
        "add_familiar_id_nombre": "PizzaFamiliar",
        "add_pizzaxl_id_nombre": "PizzaXL",
        "add_back_pizzas1": "990",
        "add_back_pizzas2": "2990",
        "add_back_pizzas3": "4990",
        "add_back_pizzas4": "6990"
    };
    total_valJson = {};
    all_pedidosJson = { "Cliente": [], "Pedido": [], "Pedido_display": [], "Cliente_wp": [], "Pedido_wp": [] };

    $("#confirm_order").hide().addClass("hide");
    $("#confirm_step1").hide();
    $("#confirm_step2").hide();
    $("#okay_order").click(function(){
        $("#contador_carrito").html();
        addAllPizzaJson = {
            "total_temporal": "0", "Total": "0",
            "Ingredientes": "(base de salsa)",
            "Pizza": "",
            "AddPizza": "",
            "add_hamburguesa_cantidad": "0",
            "add_hamburguesa_nombre": "Promo Hamburguesa",
            "add_individual_cantidad": "0",
            "add_individual_nombre": "Pizza individual",
            "add_mediana_cantidad": "0",
            "add_mediana_nombre": "Pizza Mediana",
            "add_familiar_cantidad": "0",
            "add_familiar_nombre": "Pizza familiar",
            "add_pizzaxl_cantidad": "0",
            "add_pizzaxl_nombre": "Pizza xl",
            "cantidad_total": "0",
            "cantidad_total_history": "0",
            "add_hamburguesa": "1620",
            "add_individual": "699",
            "add_mediana": "2990",
            "add_familiar": "4990",
            "add_pizzaxl": "6990",
            "add_individual_id_nombre": "PizzaIndividual",
            "add_mediana_id_nombre": "PizzaMediana",
            "add_familiar_id_nombre": "PizzaFamiliar",
            "add_pizzaxl_id_nombre": "PizzaXL",
            "add_hamburguesa_id_nombre": "Hamburguesa",
            "add_back_pizzas1": "990",
            "add_back_pizzas2": "2990",
            "add_back_pizzas3": "4990",
            "add_back_pizzas4": "6990",
            "add_back_hamburguesa": "1620"
        };
        all_pedidosJson = { "Cliente": [], "Pedido": [], "Pedido_display": [], "Cliente_wp": [], "Pedido_wp": [] };
    });
    $("#realizar_order").click(function () {
        $("#checkout_box").focus();

        $.ajax({
            url: "/_get_realizar_order",
            type: "POST",
            success: function (resp) {
                $('#order_box').html(resp.data);
                $("#realizar_order").hide().addClass("hide");
                $("#confirm_order").show().removeClass("hide");
                $(".datos_step").css("display", "flex");
                $(".pedido_step").css("width", "-webkit-fill-available");
                $(".first_step_datos").css("width", "-webkit-fill-available");

            }
        });
    });
    ShowTotal();

    $("#carrito").click(function (e) {
        $("#checkout_box").focus();

        e.preventDefault();
        if (addAllPizzaJson.cantidad_total >= 1) {
            $(".checkout_box").css("display", "flex");
            $(".div_welcome_home").hide();
            $(".contact_form").hide();
            $("body").addClass("class_background");
            //$("footer").css("position", "fixed");
        }
        else {
            $(".checkout_box").css("display", "none");
            $(".div_welcome_home").show();
            $(".contact_form").show();
            $("body").removeClass("class_background");
        }

        /*valor1 = "pedro";
        valor2 = "roberto";
        valor3 = "lagartija";
        valor4 = "elefante";
        what = "mi nombre es ";
        varjson = {};
        
        varjson["nombre"] = valor1;
        varjson["apellido"]= valor4;*/


    });
    $("#close, #closex").click(function (e) {

        e.preventDefault();
        $(".checkout_box").css("display", "none");
        $(".div_welcome_home").show();
        $(".contact_form").show();
        $("body").removeClass("class_background");


    });
    $("#close_step1").click(function (e) {

        e.preventDefault();
        $("#confirm_step1").hide().addClass("hide");
        $(".checkout_box").show().removeClass("hide");
        $("footer").css("position", "initial");

    });
    $("#close_step2").click(function (e) {

        e.preventDefault();
        $("#confirm_step2").hide().addClass("hide");
        $(".checkout_box").show().removeClass("hide");
        $("footer").css("position", "initial");

    });
    $("#confirm_order").click(function () {
        $("#checkout_box").focus();
        nombre_val = $("#nombre_cliente").val();
        localidad_val = $("#Localidad").val();
        direccion_val = $("#direccion_order").val();
        telefono_val = $("#telefono_order").val();
        formadepago_val = $("#FormaDePago").val();
        commentorder_val = $("#comment_order").val();
        all_pedidosJson["Cliente"]["Nombre"] = nombre_val;
        all_pedidosJson["Cliente"]["localidad"] = localidad_val;
        all_pedidosJson["Cliente"]["direccion"] = direccion_val;
        all_pedidosJson["Cliente"]["telefono"] = telefono_val;
        all_pedidosJson["Cliente"]["formadepago"] = formadepago_val;
        all_pedidosJson["Cliente"]["comentario"] = commentorder_val;

        $("#checkout_box").hide().addClass("hide");
        $("#confirm_step1").css("display", "flex");


    });
    $("#finish_order").click(function () {
        $("#checkout_box").focus();
        todoslospedidos = all_pedidosJson["Pedido_display"];
        $("#audiosuccess")[0].play();
        propina = $("#propina").val();
        delivery = $("#delivery").val();

        if (propina == "No") {
            propina = 0;
        }
        total = parseInt(addAllPizzaJson.Total);
        propinas = parseInt(propina);
        addAllPizzaJson["Total"] = total + propinas;
        all_pedidosJson["Cliente"]["Propina"] = $("#propina").val();
        all_pedidosJson["Cliente"]["Delivery"] = delivery;
        all_pedidosJson["Cliente_wp"] = "Su nombre es: " + all_pedidosJson["Cliente"]["Nombre"] + ", " + "su localidad es: " + all_pedidosJson["Cliente"]["localidad"] + ", Direccion: " + all_pedidosJson["Cliente"]["direccion"] + ", telefono: " + all_pedidosJson["Cliente"]["telefono"] + ", Forma de pago: " + all_pedidosJson["Cliente"]["formadepago"] + ", Comentario: " + all_pedidosJson["Cliente"]["comentario"] + ", Propina: " + all_pedidosJson["Cliente"]["Propina"] + ", Delivery: " + all_pedidosJson["Cliente"]["Delivery"] + " Pedido: " + all_pedidosJson["Pedido_wp"] + ", Tiempo de espera: " + all_pedidosJson["Pedido"]["Espera"] + ", Total: $" + addAllPizzaJson.Total;
        $("#confirm_step1").css("display", "none");
        $("#confirm_step2").css("display", "flex");
        $("#order_list").html(todoslospedidos);//aqui//
        $("#contador_buy_checkout_final").html(addAllPizzaJson.Total);
        datax = JSON.stringify(all_pedidosJson["Cliente_wp"])
        $.ajax({
            dataType: "json",
            contentType: "application/json",
            url: "/_get_order_to_send",
            method: "POST",
            data: datax
        }).done(function (resp) {
            if (resp = "ok") {
            }

        });


    });


    var $window = $(window);
    var $sector = $('#home_section_right');
    var $main = $('main');
    var $login_inputs = $('.login_main');
    var $loginopen = $("#loginopen");
    var $login = $("#login");
    function checkWidth() {
        var windowsize = $window.width();
        if (windowsize < 900) {
            //$("#home").html("<i class='fas fa-store fa-lg'></i>");
            $loginopen.hide();
            $sector.hide();
            /*$main.css("height", "10%");*/

            $login_inputs.hide();
            /*$(".types_of_pizzas").css("flex-direction", "column");
            $(".plistup").css("display", "initial");
            $(".plistdown").css("display", "initial");*/
            $("#login").click(function (e) {
                e.preventDefault();
                $loginopen.show().css("visibility", "visible");
                $login.hide();

                $login_inputs.show("Slow").css("visibility", "initial");
                $main.css("height", "200px");
            });
            $("#loginopen").click(function (e) {
                $loginopen.hide().css("visibility", "hidden");
                $login.show().css("visibility", "initial");
                e.preventDefault();

                $login_inputs.hide("Slow").css("visibility", "initial");
                $main.css("height", "50px");
            });
        } else if (windowsize > 1000) {
            //$("#home").html("Tienda<i class='fas fa-store fa-lg'></i>");
           
            /*$(".plistup").css("display", "inherit");
            $(".plistdown").css("display", "inherit");
            $(".types_of_pizzas").css("flex-direction", "row");*/
            $main.css("height", "50px");
            $sector.show();
            $login_inputs.show().removeClass("hide");
        }

    }


    checkWidth();

    $(window).resize(checkWidth);
    $(".back_pizzas").addClass("hide").hide();
    $(".add_to_pizza").addClass("hide").hide();

    $("#pizza1, #pizza2, #pizza3, #pizza4, #hamburguesa").click(function () {
        //$("#f_pizza1, #f_pizza2, #f_pizza3, #f_pizza4").hide();
        pizzas = $(this).attr("id");

        console.log(pizzas);

        $("#pizza_box").ready(function () {
            total_valJson = parseInt(Object.values(addAllPizzaJson.Total));
            $("#contador_buy").html(addAllPizzaJson.Total);
        });

        selectPizzaJson = { "pizza": "#" + pizzas };

        pizzaJson = selectPizzaJson.pizza;

        $(".title_pizzas").html("Seleccione sus ingredientes");
        allpizzas = "#pizza1, #pizza2, #pizza3, #pizza4, #hamburguesa";
        $(allpizzas).hide();
        $(pizzaJson).show();
        $(pizzaJson).addClass("type_of_pizza_select");
        allbackpizzas = "#back_pizzas1, #back_pizzas2, #back_pizzas3, #back_pizzas4";
        if (pizzaJson == "#hamburguesa") {
            total_valJson = parseInt(addAllPizzaJson.Total);
            addPizzaJson = parseInt(addAllPizzaJson.add_hamburguesa);
            total_temporalJson = parseInt(addAllPizzaJson.total_temporal);
            suma_temporalJson = total_temporalJson + addPizzaJson;
            addAllPizzaJson.total_temporal = parseInt(suma_temporalJson);
            suma_all = total_valJson + addPizzaJson;
            addAllPizzaJson.Total = suma_all;
            $("#contador_buy").html(suma_all);
            $("#hamburguesa").attr("id", "hamburguesa_open");
            $("#add_back_hamburguesa").removeClass("hide").show();
            $("#hamburguesa_all").removeClass("hide").show();
            $(".add_pizza").addClass("hide").hide();
            $("#description_hamburguesa").removeClass("hide").show().html("Arma tu hamburguesa como quieras!");
        }
        else if (pizzaJson == "#pizza1") {
            total_valJson = parseInt(addAllPizzaJson.Total);
            addPizzaJson = parseInt(addAllPizzaJson.add_individual);
            total_temporalJson = parseInt(addAllPizzaJson.total_temporal);
            suma_temporalJson = total_temporalJson + addPizzaJson;
            addAllPizzaJson.total_temporal = parseInt(suma_temporalJson);
            suma_all = total_valJson + addPizzaJson;
            addAllPizzaJson.Total = suma_all;
            $("#contador_buy").html(suma_all);
            $("#pizza1").attr("id", "pizza1_open");
            $("#add_back_pizzas1").removeClass("hide").show();
            $("#individual_all").removeClass("hide").show();
            $(".add_pizza").addClass("hide").hide();
            $("#description_individual").removeClass("hide").show().html("Pizza individual, artesanal, el diametro de esta pizza es de 20cm");
        } else if (pizzaJson == "#pizza2") {
            total_valJson = parseInt(addAllPizzaJson.Total);
            addPizzaJson = parseInt(addAllPizzaJson.add_mediana);
            total_temporalJson = parseInt(addAllPizzaJson.total_temporal);
            suma_temporalJson = total_temporalJson + addPizzaJson;
            addAllPizzaJson.total_temporal = parseInt(suma_temporalJson);
            suma_all = total_valJson + addPizzaJson;
            addAllPizzaJson.Total = suma_all;
            $("#contador_buy").html(suma_all);
            $("#pizza2").attr("id", "pizza2_open");
            $("#add_back_pizzas2").removeClass("hide").show();
            $("#mediana_all").removeClass("hide").show();
            $(".add_pizza").addClass("hide").hide();
            $("#description_mediana").removeClass("hide").show().html("Pizza Mediana, artesanal, el diametro de esta pizza es de 30cm");
        } else if (pizzaJson == "#pizza3") {
            total_valJson = parseInt(addAllPizzaJson.Total);
            addPizzaJson = parseInt(addAllPizzaJson.add_familiar);
            total_temporalJson = parseInt(addAllPizzaJson.total_temporal);
            suma_temporalJson = total_temporalJson + addPizzaJson;
            addAllPizzaJson.total_temporal = parseInt(suma_temporalJson);
            suma_all = total_valJson + addPizzaJson;
            addAllPizzaJson.Total = suma_all;
            $("#contador_buy").html(suma_all);
            $("#pizza3").attr("id", "pizza3_open");
            $("#add_back_pizzas3").removeClass("hide").show();
            $("#familiar_all").removeClass("hide").show();
            $(".add_pizza").addClass("hide").hide();
            $("#description_familiar").removeClass("hide").show().html("Pizza Familiar, artesanal, el diametro de esta pizza es de 38cm");
        } else if (pizzaJson == "#pizza4") {
            total_valJson = parseInt(addAllPizzaJson.Total);
            addPizzaJson = parseInt(addAllPizzaJson.add_pizzaxl);
            total_temporalJson = parseInt(addAllPizzaJson.total_temporal);
            suma_temporalJson = total_temporalJson + addPizzaJson;
            addAllPizzaJson.total_temporal = parseInt(suma_temporalJson);
            suma_all = total_valJson + addPizzaJson;
            addAllPizzaJson.Total = suma_all;
            $("#contador_buy").html(suma_all);
            $("#pizza4").attr("id", "pizza4_open");
            $("#add_back_pizzas4").removeClass("hide").show();
            $("#xlpizza_all").removeClass("hide").show();
            $(".add_pizza").addClass("hide").hide();
            $("#description_pizzaxl").removeClass("hide").show().html("Pizza XL, artesanal, el diametro de esta pizza es de 48cm");
        }
        ShowTotal();
    });
    $("#add_back_pizzas1, #add_back_pizzas2, #add_back_pizzas3, #add_back_pizzas4, #add_back_hamburguesa").click(function () {
        $("#f_pizza1, #f_pizza2, #f_pizza3, #f_pizza4").show();
        total_valJson = parseInt(addAllPizzaJson.Total);
        add_back_pizzas = $(this).attr("id");
        addAllPizzaJson.Ingredientes = "";
        valuePizza = parseInt(addAllPizzaJson[add_back_pizzas]);
        total_temporalJson = parseInt(addAllPizzaJson.total_temporal);

        total_valJson = parseInt(addAllPizzaJson.Total);
        restaJson = total_valJson - total_temporalJson;
        addAllPizzaJson.Total = restaJson;
        addAllPizzaJson.total_temporal = 0;
        $(":Checked").prop("checked", false);
        $("#pizza1_open").attr("id", "pizza1");
        $("#pizza2_open").attr("id", "pizza2");
        $("#pizza3_open").attr("id", "pizza3");
        $("#pizza4_open").attr("id", "pizza4");
        $("#hamburguesa_open").attr("id", "hamburguesa");

        allbackpizzas = "#back_pizzas1, #back_pizzas2, #back_pizzas3, #back_pizzas4, #back_hamburguesa";
        $("#" + add_back_pizzas).hide();
        $("#individual_all, #mediana_all, #familiar_all, #xlpizza_all, #hamburguesa_all").hide();
        $("#pizza1, #pizza2, #pizza3, #pizza4, #hamburguesa").show();
        $(".add_pizza").removeClass("hide").show();
        $(".type_of_pizza").removeClass("type_of_pizza_select");
        $(".title_pizzas").html("Seleccione todo lo que quiera llevar");
        $(".description_pizza").html("").hide().addClass("hide");
        $("#contador_buy").html(addAllPizzaJson.Total);
    });
    $("#Palta_hot, #Pepinillos_hot, #Tomate_hot, #Queso_hot, #Queso_cheddar, #Cebolla_hot, #Tocino_hot, #Huevo, #Mayonesa, #Ketchup, #Mostaza").click(function(){
        ingrediente = $(this).attr("id");
        if ($(this).is(":Checked")) {

            $("#pizza_box").ready(function () {
                total_valJson = parseInt(addAllPizzaJson.Total);
                //total_val = parseInt(Cookies.get("Total"));
                $("#contador_buy").html(addAllPizzaJson.Total);

            });

            total_valJson = parseInt(addAllPizzaJson.Total);
            total_temporalJson = parseInt(addAllPizzaJson.total_temporal);
            ingrediente = $(this).attr("id");
            selectIngJson[ingrediente] = ingrediente;
            valor_ingrediente = ingredienteJson[ingrediente];

            suma_temporalJson = total_temporalJson + valor_ingrediente;
            suma_totalJson = total_valJson + valor_ingrediente;
            addAllPizzaJson.total_temporal = suma_temporalJson;
            addAllPizzaJson.Total = suma_totalJson;


        }
        else {
            ingrediente = $(this).attr("id");
            valor_ingrediente = ingredienteJson[ingrediente];
            delete selectIngJson[ingrediente];
            total_valJson = parseInt(addAllPizzaJson.Total);
            total_temporalJson = parseInt(addAllPizzaJson.total_temporal);

            suma_totalJson = total_temporalJson - valor_ingrediente;
            addAllPizzaJson.Total = total_valJson - valor_ingrediente;
            addAllPizzaJson.total_temporal = suma_totalJson;

            $("#contador_buy").html(addAllPizzaJson.Total);

        }
    });
    $("#Muzzarella, #Pepperoni, #Aceitunas_negras, #Aceitunas_verdes, #Pimenton_rojo, #Pimenton_verde, #Cebolla, #Camarones, #Champiñones, #Salame, #Jamon, #Tomate, #Choclo, #Palta").click(function () {
        ingrediente = $(this).attr("id");
        if ($(this).is(":Checked")) {

            $("#pizza_box").ready(function () {
                total_valJson = parseInt(addAllPizzaJson.Total);
                //total_val = parseInt(Cookies.get("Total"));
                $("#contador_buy").html(addAllPizzaJson.Total);

            });

            total_valJson = parseInt(addAllPizzaJson.Total);
            total_temporalJson = parseInt(addAllPizzaJson.total_temporal);
            ingrediente = $(this).attr("id");
            selectIngJson[ingrediente] = ingrediente;
            valor_ingrediente = ingredienteJson[ingrediente];

            suma_temporalJson = total_temporalJson + valor_ingrediente;
            suma_totalJson = total_valJson + valor_ingrediente;
            addAllPizzaJson.total_temporal = suma_temporalJson;
            addAllPizzaJson.Total = suma_totalJson;


        }
        else {
            ingrediente = $(this).attr("id");
            valor_ingrediente = ingredienteJson[ingrediente];
            delete selectIngJson[ingrediente];
            total_valJson = parseInt(addAllPizzaJson.Total);
            total_temporalJson = parseInt(addAllPizzaJson.total_temporal);

            suma_totalJson = total_temporalJson - valor_ingrediente;
            addAllPizzaJson.Total = total_valJson - valor_ingrediente;
            addAllPizzaJson.total_temporal = suma_totalJson;

            $("#contador_buy").html(addAllPizzaJson.Total);

        }
    });
    $("#add_individual, #add_mediana, #add_familiar, #add_pizzaxl, #add_hamburguesa").click(function (e) {
        e.preventDefault();
        whatpizza = $(this).attr("id");
        $whatpizza = whatpizza + "_id";
        $("#audiodelete")[0].play();

        if ((whatpizza == "add_individual" | whatpizza == "add_mediana" | whatpizza == "add_familiar" | whatpizza == "add_pizzaxl") & !$(".muzza").is(":checked")) {
            $("#audioerror")[0].play();
            alert("Al menos tiene que ponerle muzzarella a su pizza!");
        } else {

            ingJs = Object.keys(selectIngJson);
            selectIngJson = {};
            ingredientesJson = JSON.stringify(ingJs).replace(/[^a-zA-Z0-9,]/g, '');
            //ingredienteJson = JSON.parse(ingredienteJson);




            cantidad_whatpizza_numb = parseInt(addAllPizzaJson[whatpizza + "_cantidad"]);

            cantidad_whatpizza_nombre = addAllPizzaJson[whatpizza + "_nombre"];

            cantidad_whatpizza_nombre_id = addAllPizzaJson[$whatpizza + "_nombre"];

            total_whatpizza = cantidad_whatpizza_numb + 1;

            addAllPizzaJson[whatpizza + "_cantidad"] = total_whatpizza;

            total_individual = parseInt(addAllPizzaJson.add_individual_cantidad);
            total_mediana = parseInt(addAllPizzaJson.add_mediana_cantidad);
            total_familiar = parseInt(addAllPizzaJson.add_familiar_cantidad);
            total_pizzaxl = parseInt(addAllPizzaJson.add_pizzaxl_cantidad);
            total_hamburguesa = parseInt(addAllPizzaJson.add_hamburguesa_cantidad);
            cantidad_total = parseInt(addAllPizzaJson.cantidad_total);
            cantidad_total_history = parseInt(addAllPizzaJson.cantidad_total_history);
            nombrepedido = cantidad_whatpizza_nombre + " " + total_whatpizza;
            PriceTempPizza = addAllPizzaJson.total_temporal;
            $idpedido = addAllPizzaJson.cantidad_whatpizza_nombre_id + total_whatpizza;
            //descripcionpedido = [cantidad_whatpizza_nombre + " n" + total_whatpizza + ": " + ingredientesJson, PriceTempPizza];
            $ingredientes = ingredientesJson;
            $pricePizza = PriceTempPizza;
            all_pedidosJson["Pedido"][nombrepedido] = [$ingredientes, $pricePizza];
            all_pedidosJson["Pedido_display"][cantidad_total_history] = ["<div id='pizza_display" + cantidad_total_history + "'" + "class='pizza_display'>" + "<div class='nombrepedido_display'>" + nombrepedido + "</div>" + "<div class='ingredientes_display'>" + "Ingredientes: " + $ingredientes + "</div>" + "<div class='valor_display'>" + " valor: $" + $pricePizza + "</div>" + "</div>"];
            all_pedidosJson["Pedido_wp"][cantidad_total_history] = nombrepedido + ", Ingredientes: " + $ingredientes + ", valor: $" + $pricePizza;
            all_pedidosJson["Total"] = parseInt(addAllPizzaJson.Total);
            //all_pedidosJson["Pedido_display"]["<div class='nombrepedido_ver'>" + nombrepedido + "</div>"]= ["<div class='ingredientes_ver'>" + $ingredientes + "</div>", "<div class='valor_ver'" + $pricePizza + "</div>"];
            //$pedido = all_pedidosJson["Pedido"][cantidad];

            $botonclose = "<div id='delete_item" + cantidad_total_history + "' class='delete_item'>Eliminar <i id='" + cantidad_total_history + "'class='fas fa-window-close'></i></div>";

            $(".checkout_list").append("<div id='" + whatpizza + "_cantidad" + "' class='pedido'>" + $botonclose + "<div id='" + nombrepedido + "' class='ingredientes_check'>" + nombrepedido + ": " + "<br>" + ingredientesJson + "</div>" + "</div>");
            var i = "delete_item" + cantidad_total_history;



            $(":Checked").prop("checked", false);

            addAllPizzaJson.total_temporal = "0";

            cantidad_total_productos = total_individual + total_mediana + total_familiar + total_pizzaxl + total_hamburguesa;

            addAllPizzaJson.cantidad_total = cantidad_total_productos;
            addAllPizzaJson.cantidad_total_history = cantidad_total_history + 1;


            $("#contador_carrito").html(addAllPizzaJson.cantidad_total);

            $("#pizza1_open").attr("id", "pizza1");
            $("#pizza2_open").attr("id", "pizza2");
            $("#pizza3_open").attr("id", "pizza3");
            $("#pizza4_open").attr("id", "pizza4");
            $("#hamburguesa_open").attr("id", "hamburguesa");
            $("#pizza1, #pizza2, #pizza3, #pizza4, #hamburguesa").show();
            $(".add_to_pizza").hide();
            $(".add_pizza").removeClass("hide").show();
            $(".description_pizza").hide();
            $(".back_pizzas").hide();
            $("#contador_buy_checkout").html(addAllPizzaJson.Total);


            waitingorder();

            $("#delete_item" + cantidad_total_history).click(function (e) {

                $("#audiodelete")[0].play();
                e.preventDefault();

                $typePizza = $(this).parent().attr("id");
                delete_item = $(this).attr("id");
                $valorTypePizza = addAllPizzaJson[$typePizza];
                //$valorTypePizza = parseInt(Cookies.get($typePizza));



                PizzaToDelete = $(this).next().attr("id");
                $PricePizzaSelect = all_pedidosJson["Pedido"][PizzaToDelete][1];
                addAllPizzaJson.Total = addAllPizzaJson.Total - $PricePizzaSelect;
                //here put delete//
                $("#contador_buy").html(addAllPizzaJson.Total);
                $("#contador_buy_checkout").html(addAllPizzaJson.Total);
                addAllPizzaJson.cantidad_total_history = addAllPizzaJson.cantidad_total_history;
                addAllPizzaJson.cantidad_total = addAllPizzaJson.cantidad_total - 1;
                addAllPizzaJson[$typePizza] = addAllPizzaJson[$typePizza] - 1;
                $("#contador_carrito").html(addAllPizzaJson.cantidad_total);

                $(this).parent().remove();//elemento del padre
                    /*DELETE */delete all_pedidosJson["Pedido"][PizzaToDelete];
                deletepedido_display = $(this).find("i").attr("id");//elemento hijo 
                delete all_pedidosJson["Pedido_display"][deletepedido_display];
                waitingorder();
                if (addAllPizzaJson.cantidad_total >= 1) {
                    $(".checkout_box").css("display", "flex");
                    $(".div_welcome_home").hide();
                    $(".contact_form").hide();
                }
                else {
                    $(".checkout_box").css("display", "none");
                    $(".datos_step").css("display", "none");
                    $(".pedido_step").css("width", "auto");
                    $(".first_step_datos").css("width", "auto");
                    $(".div_welcome_home").show();
                    $(".contact_form").show();
                    $("body").removeClass("class_background");
                    $("#realizar_order").removeClass("hide").show();
                    $("#confirm_order").hide();
                }


            });


        }

    });

}

